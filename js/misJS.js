$(function(){
    $("[data-toggle='tooltip']").tooltip();	
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 200
    });
    $('#modalRegistro').on('show.bs.modal', function(e){
        console.log('El modal se está cargando');
        $('#btnRegistro').removeClass('btn-danger');
        $('#btnRegistro').addClass('btn-white');
        $('#btnRegistro').prop('disabled', true);
    });
    $('#modalRegistro').on('shown.bs.modal', function(e){
        console.log('El modal se acabó de mostrar');
    });
    $('#modalRegistro').on('hide.bs.modal', function(e){
        console.log('El modal se está cerrando');
    });
    $('#modalRegistro').on('hidden.bs.modal', function(e){
        console.log('El modal se ocultó');
        $('#btnRegistro').removeClass('btn-danwhiteger');
        $('#btnRegistro').addClass('btn-danger');
        $('#btnRegistro').prop('disabled', false);
    });	
});